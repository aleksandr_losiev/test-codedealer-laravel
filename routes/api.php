<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Origin: http://test-losiev-vue.webdill.com');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, X-Token-Auth, Authorization, Access-Control-Allow-Origin, Access-Control-Allow-Methods');


Route::group(['middleware' => ['json.response']], function () {

    // public routes
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/register', 'Api\AuthController@register')->name('register.api');
    Route::get('/users', 'Api\UserController@getUsers')->name('users.api');

    // private routes
    Route::group(['middleware' => ['auth:api']], function () {
        Route::get('/logout', 'Api\AuthController@logout')->name('logout');

        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'Api\UserController@show')->name('user');
            Route::post('update', 'Api\UserController@update')->name('user.update');
            Route::post('image', 'Api\UserController@setImage')->name('user.setImage');
        });

        Route::group(['prefix' => 'like'], function () {
            Route::get('get', 'Api\LikeController@getLikes')->name('like.get');
            Route::post('set', 'Api\LikeController@setLike')->name('like.set');
            Route::post('delete', 'Api\LikeController@deleteLike')->name('like.delete');
        });
    });

});