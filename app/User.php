<?php

namespace App;

use Carbon\Carbon;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at'
    ];

    protected $appends = [
        'registration_date',
    ];

    public function likes()
    {
        return $this->hasMany('App\Like');
    }

    public function liked()
    {
        return $this->hasMany('App\Like', 'user_id_liked');
    }

    public function getRegistrationDateAttribute()
    {
        $date = Carbon::parse($this->attributes['created_at']);

        return $date->format('d M Y');
    }

}
