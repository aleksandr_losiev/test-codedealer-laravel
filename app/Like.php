<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
        'user_id', 'user_id_liked',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function userLiked()
    {
        return $this->belongsTo('App\User', 'user_id_liked');
    }

//    public function myLike( $my_id ) {
//        return $this->where( 'user_id', $my_id ) ? true : false;
//    }
//
//    User::with
//
//    $user->likes->myLike( $my_user_id );
}
