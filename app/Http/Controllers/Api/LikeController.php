<?php

namespace App\Http\Controllers\Api;

use App\Like;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    public function getLikes()
    {
        $user = Auth::user();

        return Like::where('user_id', '=', $user->id)->get();
    }

    public function setLike(Request $request)
    {
        $likes = Auth::user()->likes()->where('user_id_liked', $request->user_id_liked)->get();

        if ($likes->count()) {
            return response('You already liked the user before', 200);
        }

        return Like::create($request->toArray());
    }

    public function deleteLike(Request $request)
    {
        $like = Auth::user()->likes()->where('user_id_liked', $request->user_id_liked)->first();

        $answer = false;
        if ($like) {
            $answer = $like->id;
            $like->delete();
        }

        return response(['data' => $answer]);
    }
}
