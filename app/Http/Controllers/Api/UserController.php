<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\UserImageRequest;

class UserController extends Controller
{
    public function getUsers()
    {
        $users = User::select('id', 'name', 'last_name', 'image', 'created_at')->get()->toArray();

        return $users;


//        $users = User::select('id', 'name', 'last_name', 'image', 'created_at')->get();
//
//        if(  $user = Auth::user() ) {
//            foreach ($users as $user) {
//                if( $user->likes->where('user_id', $user->id )->first() ) {
//                    $user->is_liked = true;
//                }
//            }
//        }
//
//        return $users->toArray();
    }

    /**
     * Get User
     *
     * @param Request $request
     * @return mixed
     */
    public function show(Request $request)
    {
        return $request->user();
    }

    /**
     * Update User data
     *
     * @param UserRequest $request
     * @return string
     */
    public function update(UserUpdateRequest $request)
    {
        $user = Auth::user();

        $user->update($request->toArray());

        return response('Succesfully updated', 200);
    }

    public function setImage(UserImageRequest $request)
    {
        $cover = $request->file('image');
        $extension = $cover->getClientOriginalExtension();

        Storage::disk('user')->put($cover->getFilename() . '.' . $extension, File::get($cover));

        $user = Auth::user();
        $user->image = $cover->getFilename() . '.' . $extension;
        $user->save();

        return response(['status' => 'Image updated', 'url' => $cover->getFilename() . '.' . $extension], 200);
    }

}
